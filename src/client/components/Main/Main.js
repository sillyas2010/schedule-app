import React, {Component} from "react";
import { compose, bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.min.css';

import Header from '../Header/Header';
import Footer from '../Footer/Footer';
import Title from '../Title/Title';
import Routes from '../../routes';

class Main extends Component {
    state = {
        routeTitle: 'Main'
    };

    setRouteTitle = (title) => {
        if(title) {
            this.setState({
                routeTitle: title
            });
        }
    }

    render() {
        return (
            <>
              <Title pageTitle={this.state.routeTitle}/>
              <Header/>
              <Routes setRouteTitle={this.setRouteTitle}/>
              <Footer/>
              <ToastContainer position={toast.POSITION.BOTTOM_RIGHT}/>
            </>
        );
    }
}

const mapStoreToProps = store => {
    return {};
};

export default connect(mapStoreToProps)(Main);