import React, { Component } from 'react';
import { Provider } from 'react-redux';
import { HashRouter } from "react-router-dom";

import { store } from './../../common/utilities/store';
import { history } from './../../common/utilities/history';

import Main from './../Main/Main';

import './app.scss';

export default class App extends Component {
  componentDidMount() {
    setTimeout(() => {
      document.body.classList.remove('loading','active');
    }, 1000)
  }

  render() {
    return (
      <Provider store={store}>
        <HashRouter forceRefresh={true}>
            <Main/>
        </HashRouter>
      </Provider>
    );
  }
}
