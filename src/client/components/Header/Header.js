import React, {Component} from "react";
import { Link } from 'react-router-dom';

import Navigation from './../Navigation/Navigation';

import './header.scss';

class Header extends Component {
    state = {
        needsNavigation: true
    }

    render() {
        return (
            <>
                <header>
                    <div className="container">
                        <div className="row">
                            <div className={ this.state.needsNavigation ? "col-12 col-sm-2" : "col-12" }>
                                <div className="logo-wrapper">
                                    <Link to="/">
                                        <h1 className="logo">
                                            Schedule
                                        </h1>
                                    </Link>
                                </div>
                            </div>
                            {
                                this.state.needsNavigation ?

                                <Navigation className="col-12 col-sm-10"/>
                                :
                                ''
                            }
                        </div>
                    </div>
                </header>
                <div className="header-overlay"></div>
            </>
        );
    }
}

export default Header;