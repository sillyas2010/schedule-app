import React, {Component} from "react";
import { compose, bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import axios from 'axios';
import classNames from 'classnames';
import { toast } from 'react-toastify';
import { Calendar, momentLocalizer, Views } from 'react-big-calendar';
import { Link } from 'react-router-dom';
import moment from 'moment';

import PageHeading from '../PageHeading/PageHeading';

import { fetchTeachersPromise } from './../../common/utilities/api';
import { fetchAllTeachers, teachersIsIsntLoading } from './../../actions';

import './teachersList.scss';

const localizer = momentLocalizer(moment);

class TeachersList extends Component {
    componentDidMount() {
        if(!this.props.teachers.length) {        
            this.props.teachersIsIsntLoading(true);
            fetchTeachersPromise((res) => {
                if(res && res.data) {
                    this.props.fetchAllTeachers([ ...res.data]);
                }
            }, (err) => {
                this.props.teachersIsIsntLoading(false);
                toast.error('Error!');
            })
        }
    }

    render() {
        return (
            <div className={classNames("teachers-list-wrapper", { 'loading active': this.props.isLoading })}>
                <PageHeading title={'Teachers'} 
                    description={'list'}
                />

                <ul>
                    {
                        this.props.teachers.map((teacher) => (
                            <li key={`${teacher.firstName}_${teacher.id}`}>
                                <Link to={"/teacher/" + teacher.id}>{teacher.firstName} {teacher.lastName}</Link>
                            </li>
                        ))
                    }
                </ul>

                { false &&
                    <div>
                        <Calendar
                        localizer={localizer}
                        events={[]}
                        startAccessor="start"
                        endAccessor="end"
                        defaultView={Views.WORK_WEEK}
                        views={[Views.WEEK, Views.WORK_WEEK]}
                        />
                    </div>
                }
            </div>
        );
    }
}

const mapStoreToProps = store => {
    let { all, isLoading } = store.teachersReducer;
    return {
        teachers: all.map((teacher) => {
            return {
                ...teacher,
                events: teacher.events.map((event) => {
                    let newEvent = { ...event };

                    if(event.start) {
                        newEvent.start = new Date(event.start);
                    }

                    if(event.end) {
                        newEvent.end = new Date(event.end);
                    }

                    return newEvent;
                })
            }
        }) ,
        isLoading
    };
};
const mapDispatchToProps = (dispatch) => {
    return bindActionCreators({
        fetchAllTeachers,
        teachersIsIsntLoading
    }, dispatch)
};

export default connect(mapStoreToProps, mapDispatchToProps)(TeachersList);