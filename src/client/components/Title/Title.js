import React, { Component } from 'react';
import ReactDOM from 'react-dom';

class Title extends Component {
    constructor(props) {
        super(props);
        this.titleEl = document.getElementsByTagName("title")[0];
        while (this.titleEl.firstChild) {
            this.titleEl.removeChild(this.titleEl.firstChild);
        }
    }

    render() {
        let fullTitle;
        if(this.props.pageTitle) {
            fullTitle = this.props.siteTitle + " | " + this.props.pageTitle;
        } else {
            fullTitle = this.props.siteTitle;
        }

        return ReactDOM.createPortal(
            fullTitle || "",
            this.titleEl
        );
    }
}
Title.defaultProps = {
    pageTitle: null,
    siteTitle: "Schedule",
};

export default Title;