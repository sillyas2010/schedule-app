import React, {PureComponent} from "react";
import { Link } from 'react-router-dom';
import classNames from 'classnames';

import './navigation.scss';

class Navigation extends PureComponent {
    list = [
        { title: 'Discover', link: '/' }
    ]

    render() {
        return (
            <div className={this.props.className}>
                <div className="header-navigation-wrapper">
                    {
                        this.list.map((el, ind) => (
                            <div className="navigation-link-wrapper" key={`${el.title}_${ind}`}>
                                <Link to={el.link}>{el.title}</Link>
                            </div>
                        ))
                    }
                </div>
            </div>
        );
    }
}

export default Navigation;