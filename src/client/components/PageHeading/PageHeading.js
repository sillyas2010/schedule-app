import React, {PureComponent} from "react";
import { compose, bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import classNames from 'classnames';

import './pageHeading.scss';

class PageHeading extends PureComponent {
    render() {
        const { title, description } = this.props;

        return (
            <div className="page-heading-wrapper">
                <div className="heading-title">{title}</div>
                <div className="heading-description">{description}</div>
            </div>
        );
    }
}

const mapStoreToProps = store => {
    return {};
};

const mapDispatchToProps = (dispatch) => {
    return bindActionCreators({
    }, dispatch)
};

export default connect(mapStoreToProps/*, mapDispatchToProps*/)(PageHeading);