import React, {Component} from "react";
import { compose, bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import classNames from 'classnames';
import { Calendar, momentLocalizer, Views } from 'react-big-calendar'
import moment from 'moment'

import TeachersList from '../TeachersList/TeachersList';

import './home.scss';

const localizer = momentLocalizer(moment);

class Home extends Component {
    render() {
        return (
            <>
                <main>
                    <section className="schedule-block-wrapper">
                        <div className="container">
                            <div className="row">
                                <div className="col-12">
                                    <TeachersList/>
                                </div>
                            </div>
                        </div>
                    </section>
                </main>
            </>
        );
    }
}

const mapStoreToProps = store => {
    return {};
};

const mapDispatchToProps = (dispatch) => {
    return bindActionCreators({
    }, dispatch)
};

export default connect(mapStoreToProps/*, mapDispatchToProps*/)(Home);