import React, {Component} from "react";
import { compose, bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import axios from 'axios';
import classNames from 'classnames';
import { toast } from 'react-toastify';
import { Calendar, momentLocalizer, Views } from 'react-big-calendar';
import moment from 'moment'

import PageHeading from '../PageHeading/PageHeading';

import { fetchTeachersPromise } from '../../common/utilities/api';
import { fetchAllTeachers, teachersIsIsntLoading } from '../../actions';

import './teacherDetail.scss';

const localizer = momentLocalizer(moment);

const findCurrentTeacher = (teachers, id) => {
    return teachers.find((item) => item.id === parseInt(id, 10)) || null;
}

class TeachersDetail extends Component {
    static defaultProps = {
        currentTeacher: {}
    }

    componentDidMount() {
        if(!this.props.teachers.length) {        
            this.props.teachersIsIsntLoading(true);
            fetchTeachersPromise((res) => {
                if(res && res.data) {
                    this.props.fetchAllTeachers([ ...res.data]);
                }
            }, (err) => {
                this.props.teachersIsIsntLoading(false);
                toast.error('Error!');
            })
        } else {
            this.props.teachersIsIsntLoading(false);
        }
    }

    render() {
        let { currentTeacher } = this.props,
            isTeacherFound = !!currentTeacher.id;

        return (
            <main>
                <section className="schedule-block-wrapper">
                    <div className="container">
                        <div className="row">
                            <div className="col-12">
                                <div className={classNames("teachers-detail-wrapper", { 'loading active': this.props.isLoading })}>
                                    <PageHeading title={'Teacher'} 
                                        description={isTeacherFound ? (`${currentTeacher.firstName} ${currentTeacher.lastName}`) : 'is missing'}// `
                                    />
                    
                                    <div>
                                        <Calendar
                                        localizer={localizer}
                                        events={isTeacherFound ? currentTeacher.events : []}
                                        startAccessor="start"
                                        endAccessor="end"
                                        defaultView={Views.WORK_WEEK}
                                        views={[Views.WEEK, Views.WORK_WEEK]}
                                        />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </main>
        );
    }
}

const mapStoreToProps = (store, currentProps) => {
    let { match: { params } } = currentProps,
        { all, isLoading } = store.teachersReducer,
        teachersList = all.map((teacher) => {
            return {
                ...teacher,
                events: teacher.events.map((event) => {
                    let newEvent = { ...event };

                    if(event.start) {
                        newEvent.start = new Date(event.start);
                    }

                    if(event.end) {
                        newEvent.end = new Date(event.end);
                    }

                    return newEvent;
                })
            }
        }),
        currentTeacher = findCurrentTeacher(teachersList, params.teacherId);

    return {
        teachers: teachersList,
        currentTeacher: currentTeacher || {},
        isLoading
    };
};
const mapDispatchToProps = (dispatch) => {
    return bindActionCreators({
        fetchAllTeachers,
        teachersIsIsntLoading
    }, dispatch)
};

export default connect(mapStoreToProps, mapDispatchToProps)(TeachersDetail);