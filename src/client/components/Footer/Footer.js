import React, {Component} from "react";
import { compose, bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import classNames from 'classnames';

import './footer.scss';

class Footer extends Component {
    supportMail = 'sillyas2010@gmail.com'

    state = {
        footerRef: null
    }

    componentDidMount() {
        this.setState({
            footerRef: this.footerRef
        })
    }

    setFooterRef = (node) => {
        this.footerRef = node;
    }

    render() {        
        let { isFull, onlyMobile } = this.props;

        return (
            <>
                <footer className={classNames({ "d-md-none": onlyMobile }, { "footer-full": isFull})}>
                    <div className="container">
                        <div className="row">
                            <div className={classNames("col-12", { "col-sm-6": !isFull })}>
                            </div>
                            <div className={classNames("col-12 col-sm-6", { "hidden": isFull })}>
                                <div className="support-mail-wrapper">
                                    <a href={`mailto:${this.supportMail}`} className="support-mail">support</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </footer>
                <div className={classNames("footer-overlay", { "d-md-none": onlyMobile })}></div>
            </>
        );
    }
}

const mapStoreToProps = store => {
    return {};
};

const mapDispatchToProps = (dispatch) => {
    return bindActionCreators({
    }, dispatch)
};

export default connect(mapStoreToProps/*, mapDispatchToProps*/)(Footer);