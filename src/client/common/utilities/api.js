import axios from 'axios';

import config from './config';

export const fetchTeachersPromise = (resolve, reject, filters) => {
    axios.get(`${config.API_ENDPOINT}/entities/teachers`, { params: { filters } })
    .then((res) => {
        if(resolve) {
            resolve(res);
        }
    })
    .catch((err) => {
        if(reject) {
            reject(err);
        }
    })
}