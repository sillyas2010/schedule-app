import { combineReducers } from 'redux';

import teachersReducer from './teachers';

export default combineReducers({ teachersReducer });