import * as actions from '../actions/types';

const defaultTeachersState = {
    all : []
};

export default function teachersReducer(state = defaultTeachersState, action) {
    switch (action.type) {
        case actions.FETCH_ALL_TEACHERS:
                return { ...state, all: action.teachers };
        default:
            return state;
    }
}