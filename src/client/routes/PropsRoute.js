import React, { Component } from 'react';
import { Route } from 'react-router-dom';

export default
class PropsRoute extends Component {
    componentDidMount() {
      if(typeof this.props.onEnter === 'function') {
        this.props.onEnter();
      }
    }

    render() {
        const {
          component: Component,
          ...props
        } = this.props;

        return (
            <Route
              {...props}
              render={props => (
                <Component {...props} {...this.props}/>
              )}
            />
        )
    }
}
