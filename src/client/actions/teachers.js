import {
    LOADING_TEACHERS,
    FETCH_ALL_TEACHERS
} from './types';

export const teachersIsIsntLoading = (isLoading) => ({
    type: LOADING_TEACHERS,
    isLoading: isLoading
});
export const fetchAllTeachers = (teachers) => ({
    type: FETCH_ALL_TEACHERS,
    isLoading: false,
    teachers: teachers
});
export const fetchFilteredTeachers = (teachers) => ({
    type: FETCH_FILTERED_TEACHERS,
    isLoading: false,
    teachers: teachers
});