const Sequelize = require('sequelize'),
    Model = Sequelize.Model,
    sequelize = require('../db'),
    { eventFields } = require('../common/const');

class Teacher extends Model {};
Teacher.init({
  // attributes
  firstName: {
    type: Sequelize.STRING,
    allowNull: false
  },
  lastName: {
    type: Sequelize.STRING
    // allowNull defaults to true
  },
  events: {
    type: Sequelize.ARRAY(Sequelize.STRING),
    defaultValue: [],
    get() {
      return resultEvents = (this.getDataValue('events')).map((item) => {
        let splittedItem = item.split(';'),
          eventsArray = splittedItem.reduce((prevObject, fieldValue, index) => {
          let fieldTitle = '';

          if(eventFields[index]) {
            fieldTitle = eventFields[index][0];
          }

          if(fieldTitle) {
            let fieldType = eventFields[index][1];

            if(!fieldType || fieldType === 'string') {
              prevObject[fieldTitle] = fieldValue;
            } else {
              try {
                if(fieldType === 'date') {
                  prevObject[fieldTitle] = new Date(fieldValue);
                } else if(fieldType === 'boolean') {
                  prevObject[fieldTitle] = (fieldValue == 'true');
                } else if(fieldType === 'int') {
                  prevObject[fieldTitle] = parseInt(fieldValue, 10);
                } else {
                  prevObject[fieldTitle] = fieldValue;
                }
              } catch(e) {
                prevObject[fieldTitle] = fieldValue;
              }
            }
          }

          return prevObject;
        }, {});

        return eventsArray;
      });
    },
    set(events) {
      if(events) {
        if(Array.isArray(events)) {
          let resultArray = [ ...events ].map((item) => {
            if(typeof item === 'string') {
              return item;
            } else if(typeof item === 'object') {
              return eventFields.reduce((resultString, field) => {
                resultString += `${item[field[0]] || ''};`;

                return resultString;
              }, '');
            }
          });
          // 'this' allows you to access attributes of the instance
          this.setDataValue('events', resultArray);
        } else {
          throw new Error("Events must be Array");
        }
      }
    }
  }
}, {
  sequelize,
  modelName: 'teacher'
  // options
});

module.exports = Teacher;