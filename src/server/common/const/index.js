module.exports = {
    eventFields: [
        ['id','string'], 
        ['title', 'string'], 
        ['desc', 'sting'], 
        ['allDay', 'boolean'], 
        ['start', 'date'], 
        ['end', 'date']
    ]
};