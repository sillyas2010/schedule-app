const faker = require('faker');

module.exports = {
    addProvidedFields: (bodyObj, rawModelFields, fieldsFilter) => {
        let modelFields = Object.keys(rawModelFields);

        return resultObj = (Object.keys(bodyObj)).reduce((prevObject, bodyKey) => {
            if(modelFields.indexOf(bodyKey) > -1 && fieldsFilter.indexOf(bodyKey) === -1) {
                prevObject[bodyKey] = bodyObj[bodyKey];
            }

            return prevObject;
        }, {})
    },
    times: (count, callback, ...cbArgs) => {
        let i = 0,
            result = [];
        while (i < count) {
            result.push(callback.apply(this, cbArgs));
            i += 1;
        }

        return result
    },
    generateEvent: () => {
        let date1 = faker.date.recent(7),
            date2 = faker.date.recent(4)

        return {
            id: faker.random.uuid(),
            title: faker.random.words(2),
            desc: faker.random.words(4),
            allDay: faker.random.boolean(),
            start: date1 < date2 ? date1 : date2,
            end: date1 > date2 ? date1 : date2
        };
    }
};