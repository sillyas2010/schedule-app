const { Sequelize } = require('sequelize'),
    rootDir = (__dirname).split('/src')[0],
    env = require('dotenv').config({ path: rootDir + '/.env' });

const { DB_HOST, DB_PORT, DB_TITLE, DB_USERNAME, DB_PASSWORD } = process.env;

// create a unique, global symbol name
// -----------------------------------

const dbKey = Symbol.for("schedule.app.db");

// check if the global object has this symbol
// add it if it does not have the symbol, yet
// ------------------------------------------

var globalSymbols = Object.getOwnPropertySymbols(global);
var hasDb = (globalSymbols.indexOf(dbKey) > -1);

if (!hasDb){
    global[dbKey] = new Sequelize(DB_TITLE, DB_USERNAME, DB_PASSWORD, {
        host: DB_HOST,
        port: DB_PORT,
        dialect: 'postgres'
    });
}

// define the singleton API
// ------------------------

var singleton = {};

Object.defineProperty(singleton, "instance", {
  get: function(){
    return global[dbKey];
  }
});

// ensure the API is never changed
// -------------------------------

Object.freeze(singleton);

// export the singleton API only
// -----------------------------

module.exports = singleton.instance;