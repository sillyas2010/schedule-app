const express = require('express'),
      api = require('./api'),
      sequelize = require('./db')
      fs = require('fs'),
      rootDir = (__dirname).split('/src')[0],
      env = require('dotenv').config({ path: rootDir + '/.env' }),
      bodyParser = require('body-parser');

const app = express();

app.use(express.static('dist'));
// parse application/json
app.use(bodyParser.json());

app.use('/api', api);

app.get('/', (req, res) => {
    fs.readFile(__dirname + '/index.html', 'utf8', (err, text) => {
        res.send(text);
    });
});

sequelize.sync({alter: true}).then(() => {
    console.log('db connected');
})
.catch(err => {
    console.error('Unable to connect to the database:', err);
});

app.listen(process.env.PORT || 8080, () => console.log(`Listening on port ${process.env.PORT || 8080}!`));
