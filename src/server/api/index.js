const express = require('express'),
  { Teacher } = require('../models'),
  faker = require('faker'),
  rootDir = (__dirname).split('/src')[0],
  env = require('dotenv').config({ path: rootDir + '/.env' }),
  app = express(),
  router = express.Router(),
  { times, generateEvent } = require('../common/utils'),
  entities = require('./entities');

router.get('/generate-data', (req, res) => {
  let teachersArr = times(10, () => ({
    firstName: faker.name.firstName(),
    lastName: faker.name.lastName(),
    events: times(
      faker.random.number({min: 1, max: 10}), 
      () => (generateEvent())
    )
  }))

  Teacher.bulkCreate(
    teachersArr
  ).then((result) => {
    res.status(200).json(result);
  }).catch((error) => {
    res.status(500).json({ 'err': error });
  });
})

router.use('/entities/', entities);

module.exports = router;