const express = require('express'),
  models = require('../models/index'),
  rootDir = (__dirname).split('/src')[0],
  env = require('dotenv').config({ path: rootDir + '/.env' }),
  { addProvidedFields } = require('../common/utils');
  router = express.Router();

let modelKeys = Object.keys(models);

modelKeys.forEach((modelKey) => {
    let modelTitle = modelKey.toLowerCase(),
        filterFields = ['createdAt', 'id', 'updatedAt', '__proto__'];

    router.get(`/${modelTitle}s`, (req, res) => {
        models[modelKey].findAll()
        .then( (result) => res.json(result))
        .catch( (error) => res.status(500).json(error));
    });

    router.get( `/${modelTitle}/:id`, (req, res) => {
        models[modelKey].findByPk(req.params.id)
        .then( (result) => res.json(result))
        .catch( (error) => res.status(500).json(error));
    });

    router.post(`/${modelTitle}`, (req, res) => {
        models[modelKey].create(addProvidedFields(req.body, models[modelKey].rawAttributes, filterFields))
        .then( (result) => res.json(result) )
        .catch( (error) => res.status(500).json(error));
    });

    router.put(`/${modelTitle}/:id`, (req, res) => {
        models[modelKey].update(
        addProvidedFields(req.body, models[modelKey].rawAttributes, filterFields),
        {
            where: {
                id: req.params.id
            }
        })
        .then( (result) => res.json(result) )
        .catch( (error) => res.status(500).json(error));
    });

    router.delete(`/${modelTitle}/:id`, (req, res) => {
        models[modelKey].destroy({
            where: {
                id: req.params.id
            }
        })
        .then( (result) => res.json(result) )
        .catch( (error) => res.status(500).json(error));
    });
});

module.exports = router;